var
    gulp = require('gulp'), // Gulp JS
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'), // Минификация CSS
    imagemin = require('gulp-imagemin'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    browserSync = require('browser-sync').create(),
    gulp = require('gulp'),
    reload = browserSync.reload;

// К О М А Н Д Ы
// ⛏ для запуска локального сервера:   gulp server
// 🚀 для сборки проекта:   gulp www


// С Е Р В Е Р
gulp.task('server', ['jade','stylus','js','images'], function() {
    browserSync.init({
        notify: false,
        server: "./server/"
    });
    gulp.watch("./assets/layout/**/*", ['jade','stylus']);
    gulp.watch("./assets/img/**/*", ['img']);
    gulp.watch("./assets/js/**/*", ['js']);
});
// Собираем HTML
gulp.task('jade', function() {
    gulp.src(['./assets/layout/index.jade']) // Главная страница
        .pipe(jade({pretty: true}))  
        .on('error', console.log) 
    .pipe(gulp.dest('./server/')) 
    .pipe(reload({stream: true}));

    gulp.src(['./assets/layout/page_2/index.jade']) // 2 страница
        .pipe(jade({pretty: true}))  
        .on('error', console.log) 
    .pipe(gulp.dest('./server/page_2/')) 
    .pipe(reload({stream: true}));

    gulp.src(['./assets/layout/page_3/index.jade']) // 3 страница
        .pipe(jade({pretty: true}))  
        .on('error', console.log) 
    .pipe(gulp.dest('./server/page_3/')) 
    .pipe(reload({stream: true}));

}); 
// Собираем CSS
gulp.task('stylus', function() {
    gulp.src('./assets/layout/**/*.styl')
        .pipe(stylus({
            // use: ['nib']
            set:[{ 'linenos': true }]
        }))
    .on('error', console.log)
    .pipe(myth()) // добавляем префиксы 
    .pipe(gulp.dest('./server/css'))
    .pipe(reload({stream: true}));
   
});
// Собираем JS
gulp.task('js', function() {
    gulp.src(['./assets/js/**/*.js', '!./assets/js/**/*.js'])
        .pipe(concat('index.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/**
        .pipe(gulp.dest('./server/js'))
        .pipe(reload({stream: true}));
});
// Копируем и минимизируем изображения
gulp.task('images', function() {
    gulp.src('./assets/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./server/img'))
        .pipe(reload({stream: true}));

});


// Р Е Л И З
gulp.task('www', function() {
    // CSS
    gulp.src('./assets/layout/index.styl')
        .pipe(stylus({
            // use: ['nib']
            set:[{ 'linenos': true }]
        }))
    .pipe(myth()) // добавляем префиксы
    // .pipe(csso()) // минимизируем
    .pipe(gulp.dest('./www/css/'))

    // index.html
    gulp.src(['./assets/layout/index.jade'])
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('./www/'))

    // page_2/index.html
    gulp.src(['./assets/layout/page_2/index.jade'])
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('./www/page_2/'))

    // page_3/index.html
    gulp.src(['./assets/layout/page_3/index.jade'])
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('./www/page_3/'))

    // js
    gulp.src(['./assets/js/**/*'])
        .pipe(concat('index.js')) // Собираем все JS в index.js
        .pipe(uglify())
        .pipe(gulp.dest('./www/js'));

    // image
    gulp.src('./assets/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./www/img'))

});
